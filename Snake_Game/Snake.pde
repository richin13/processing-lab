class Snake {

  ArrayList<PVector> tail;

  // Direction controllers
  PVector direction;

  // Head position
  int x;
  int y;

  Snake() {
    tail = new ArrayList<PVector>();

    direction = new PVector(1, 0);

    x = width / 2;
    y = height / 2;
  }

  boolean eat() {
    boolean ate = dist(x, y, food.x, food.y) < 1;

    if (ate) {
      score++;
    }

    return ate;
  }
  
  boolean died() {
    // Check if there is a collition with the tail
    for(PVector v : tail) {
      if(dist(x, y, v.x, v.y) < 1) {
        return true;
      }
    }
    
    if(x != constrain(x, 0, width - SCALE) || y != constrain(y, 0, height - SCALE)) {
      return true;
    }
    
    return false;
  }

  void dir(int x, int y) {
    if (score > 0) {
      if (x + direction.x != 0 || y + direction.y != 0) {
        direction = new PVector(x, y);
      }
    } else {
      direction = new PVector(x, y);
    }
  }

  boolean move() {
    if (score > 0) {
      // Then we need to populate the tail
      if(score == tail.size() && !tail.isEmpty()) {
        tail.remove(0);
      }
      
      tail.add(new PVector(x, y));
    }


    x = (int) (x + direction.x * SCALE);
    y = (int) (y + direction.y * SCALE);

    //x = constrain(x, 0, width - SCALE);
    //y = constrain(y, 0, height - SCALE);
    
    return died();
  }

  void show() {
    noStroke();
    fill(0, 190, 50);
    for (PVector v : tail) {
      rect(v.x, v.y, SCALE, SCALE);
    }

    rect(x, y, SCALE, SCALE); // The head
  }
}