final int SCALE = 20; // Grid size
int score = 0;
Snake s;
PVector food;

boolean paused = true;

void setup() {
  size(600, 600);
  frameRate(10);
  //

  s = new Snake();
  createFood();
}

void draw() {
  background(80);
  if (!paused) {

    if (s.eat()) {
      createFood();
    }

    if (s.move()) {
      println("Game over");
      s = new Snake();
      score = 0;
    }
    s.show();

    // Food
    fill(255);
    rect(food.x, food.y, SCALE, SCALE);

    // Text
    textSize(32);
    text("Score: " + score, (width / 2) - 75, 32);
  } else {
    text("Click to begin", 0, height);
  }
}

void mousePressed() {
  paused = false;
}

void keyPressed() {
  if (keyCode == UP) {
    s.dir(0, -1);
  } else if (keyCode == DOWN) {
    s.dir(0, 1);
  } else if (keyCode == LEFT) {
    s.dir(-1, 0);
  } else if (keyCode == RIGHT) {
    s.dir(1, 0);
  }
}

void createFood() {
  int rows = width / SCALE;
  int cols = height / SCALE;

  food = new PVector(floor(random(rows)), floor(random(cols)));
  food.mult(SCALE);
}