from PascalTriangle import PascalTriangle

_HEIGHT = 15
_t = None

def setup():
    size(800, 800)

    global _t
    _t = PascalTriangle(_HEIGHT)
    _t.generate()
    
def draw():
    background(255)
    _t.show()
    
def mouseClicked():
    global _HEIGHT
    if mouseButton == LEFT:
        _HEIGHT += 1
    if mouseButton == RIGHT:
        _HEIGHT -= 1
    _t.generate(_HEIGHT)