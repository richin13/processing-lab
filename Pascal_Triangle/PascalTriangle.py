class PascalTriangle:
    def __init__(self, height_):
        self.height_ = height_
        self.triangle_ = []
        
    def generate(self, h=None):
        self.triangle_ = []
        if h:
            self.height_ = h
        
        for i in range(1, self.height_ + 1):
            self.triangle_.append([1] * i)
            
        for i in range(1, self.height_ - 1):
            for j in range(len(self.triangle_[i]) - 1):
                self.triangle_[i+1][j+1] = self.triangle_[i][j] + self.triangle_[i][j+1]
                
    def show(self):
        scl = width / (self.height_)
        for y, l in enumerate(self.triangle_[::-1]):
            offset = (scl/2) * y
            for x, e in enumerate(l):
                fill(51)
                _y = (height-scl) - (y * scl)
                _x = offset + (x * scl)
                rect(_x, _y, scl, scl)

        for y, l in enumerate(self.triangle_[::-1]):
            offset = (scl/2) * y
            for x, e in enumerate(l):
                fill(255)
                _x = (x * scl) + (scl / 2)
                _x += offset
                text(str(e), _x, (height - (y * scl)) - (scl/2))
